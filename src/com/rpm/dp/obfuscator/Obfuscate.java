package com.rpm.dp.obfuscator;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

class Obfuscate {
    private String startPath;
    private String finPath;

    Obfuscate(String startWay, String finWay) {
        this.startPath = startWay;
        this.finPath = finWay;
    }

    void obfuscate() throws IOException {
        String listLine = fileReader(startPath);

        String multiLine = deleteComments(deleteGaps(listLine));

        String startFileName = getFileName(startPath);
        String finFileName = getFileName(finPath);

        String renameMultiline = replaceFileName(multiLine, startFileName, finFileName);

        fileRecorder(renameMultiline, finPath);
    }

    /**
     * Считывает файл по указанному пути
     *
     * @param startPath начальный абсолютный путь к файлу
     * @return файл в мультистроке
     */
    private static String fileReader(String startPath) {
        StringBuilder listLine = new StringBuilder();
        try {
            List<String> list = Files.readAllLines(Paths.get(startPath));
            //получение мультистроки
            for (String s : list) {
                listLine.append(s); //listLine += s
            }
        } catch (IOException e) {
            System.out.println("not found");
        }
        return listLine.toString();
    }

    /**
     * Удаляет комментарии
     *
     * @param listLine файл в мультистроке
     * @return мультистроку без комментариев
     */
    private static String deleteComments(String listLine) {
        return listLine.replaceAll("(/\\*.+?\\*/)|(//.+?)[:;a-zA-Zа-яА-ЯЁё]*", "");
    }


    /**
     * Меняет имя класса в файле
     *
     * @param multiLine     мультистрока без комментариев
     * @param startFileName начальное имя файла
     * @param finFilePath    конечное имя файла
     * @return измененную строку с измененным именем класса в мультисроке
     */
    private static String replaceFileName(String multiLine, String startFileName, String finFilePath) {
        return multiLine.replaceAll(startFileName, finFilePath);
    }

    /**
     * Удаляет лишние пробелы
     *
     * @param listLine мультистрока без комментариев
     * @return очищенную мультистроку
     */
    private static String deleteGaps(String listLine) {
        return listLine.replaceAll("\\s+(?![^\\d\\s])", "");
    }

    /**
     * Получает имя из пути файла без расширения
     *
     * @param filePath путь файла
     * @return файл без расширения
     */
    private static String getFileName(String filePath) {
        Path p = Paths.get(filePath);
        String fileName = p.getFileName().toString();
        return fileName.replaceAll("\\..*", "");
    }

    /**
     * Записывает мультистроку в файл
     *
     * @param renameMultiline мультистрока с измененным Main
     * @param newFileFullPath  путь к файлу с новым именем
     * @throws IOException ошибку
     */
    private static void fileRecorder(String renameMultiline, String newFileFullPath) throws IOException {
        try (FileWriter writer = new FileWriter(newFileFullPath)) {
            writer.write(renameMultiline);
        }
    }
}
