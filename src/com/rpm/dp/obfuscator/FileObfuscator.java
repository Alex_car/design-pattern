package com.rpm.dp.obfuscator;

import java.io.IOException;
import java.util.Scanner;


/**
 * Класс для реализации мини-обфускатора
 *
 * @author Mashina A.A.
 */
public class FileObfuscator {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        System.out.println("Введите абсолютный путь к файлу (с именем файла)");
        String startPath = scanner.nextLine(); //"src\\com\\rpm\\dp\\obfuscator\\Main.java";

        System.out.println("Введите абсолютный путь куда сохранить файл (можно изменить имя файла)");
        String finPath = scanner.nextLine(); //"src\\com\\rpm\\dp\\obfuscator\\MeowMain.java";

        Obfuscate facadeObfuscate = new Obfuscate(startPath, finPath);
        facadeObfuscate.obfuscate();
    }
}