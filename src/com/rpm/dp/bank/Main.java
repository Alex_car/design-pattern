package com.rpm.dp.bank;

import java.util.Scanner;

/**
 * класс для представления банковского аккаунта
 *
 * @Author Mashina
 */
public class Main {
    private static Scanner scanner = new Scanner(System.in);
    private static final Account ACCOUNT_SBERBANK = new Account("1234234534564567", "Ololo Ololoev");
    private static final Account ACCOUNT_VTB = new Account("888005553535", "Olava Okalava");

    private static final Account.Card CARD_SBERBANK_VISA = ACCOUNT_SBERBANK.new Card("2340 7845 8956 8906");
    private static final Account.Card CARD_SBERBANK_MAESTRO = ACCOUNT_SBERBANK.new Card("2340 5553 5697 3295");
    private static final Account.Card CARD_VTB_MIR = ACCOUNT_VTB.new Card("4832 5463 1597 8513");

    public static void main(String[] args) {
        int end;
        do {
            int chosenBank = choosingBank();
            int chosenCard = choosingCard(chosenBank);
            Account userWorkingNowBank = returnUserWorkingNowBank(chosenBank);
            Account.Card userWorkingNowCard = returnUserWorkingNowCard(chosenBank, chosenCard);

            System.out.println("1) Withdraw money\n2) Deposit money");
            int withdrawOrDeposit = scanner.nextInt();

            int moneyToWithdrawOrDeposit = howMuchToWithdrawOrDeposit(withdrawOrDeposit);

            checkingBank(moneyToWithdrawOrDeposit, withdrawOrDeposit, userWorkingNowBank, userWorkingNowCard);

            System.out.println("\n1) Perform another action\n2) Exit");
            end = scanner.nextInt();
        } while (end == 1);
    }

    /**
     * Предоставляет выбор банка
     *
     * @return номер банка выбранного пользователем
     */
    private static int choosingBank() {
        System.out.println("Choose bank:\n1. Sberbank\n2. VTB");
        return scanner.nextInt();
    }

    /**
     * Предоставляет выбор карты банка
     *
     * @param chosenBank банк, выбранный пользователем
     * @return номер карты банка, выбранной пользователем
     */
    private static int choosingCard(int chosenBank) {
        System.out.println("Choose a card:");
        if (chosenBank == 1) {
            System.out.println("1. Sberbank Visa\n2. Sberbank Maestro");
        }
        if (chosenBank == 2) {
            System.out.println("1. VTB Mir");
        }
        if (chosenBank > 2) {
            System.out.println("Card not found");
            return -1;
        }
        return scanner.nextInt();
    }

    /**
     * Позволяет идентифицировать банк для работы со картами
     *
     * @param chosenBank банк, выбранный пользователем
     * @return банк, с которым пользователь будет работать в данный момент
     */
    private static Account returnUserWorkingNowBank(int chosenBank) {
        if (chosenBank == 1) {
            return ACCOUNT_SBERBANK;
        }
        if (chosenBank == 2) {
            return ACCOUNT_VTB;
        }
        return new Account("00000000", "Error Bank");
    }

    /**
     * Позволяет идентифицировать карту банка для работы со счетами
     *
     * @param chosenBank банк, который выбрал пользователь
     * @param chosenCard карта банка, которую выбрал пользователь
     * @return карту банка, с которой пользователь будет работать в данный момент
     */
    private static Account.Card returnUserWorkingNowCard(int chosenBank, int chosenCard) {
        if (chosenBank == 1) {
            if (chosenCard == 1) {
                return CARD_SBERBANK_VISA;
            }
            if (chosenCard == 2) {
                return CARD_SBERBANK_MAESTRO;
            }
        }
        if (chosenBank == 2) {
            if (chosenCard == 1) {
                return CARD_VTB_MIR;
            }
        }
        return ACCOUNT_SBERBANK.new Card("00000000");
    }

    /**
     * Принимает значение желаемой суммы пользователя на пополнение/снятие
     *
     * @param withdrawOrDeposit действие пользователя с картами (пополнение/снятие)
     * @return сумма пополнения/снятия
     */
    private static int howMuchToWithdrawOrDeposit(int withdrawOrDeposit) {
        if (withdrawOrDeposit == 1) {
            System.out.print("How much to withdraw: ");
        }
        if (withdrawOrDeposit == 2) {
            System.out.print("How much to deposit: ");
        }
        return scanner.nextInt();
    }

    /**
     * Сообщает пользователю результат его действий (пополнения/снятия) определенной суммы
     *
     * @param MoneyToWithdrawOrDeposit деньги для пополнения/снятия
     * @param withdrawOrDeposit        пополнение/снятие
     * @param userWorkingNowBank       банк, с которым в данный моент работает пользователь
     * @param userWorkingNowCard       карта банка, с которой в данный момент работает пользователь
     */
    private static void checkingBank(int MoneyToWithdrawOrDeposit, int withdrawOrDeposit, Account userWorkingNowBank, Account.Card userWorkingNowCard) {
        System.out.println("Start balance: " + userWorkingNowBank.getAmount());

        if (withdrawOrDeposit == 1) {
            int tempWithdraw = userWorkingNowCard.withdraw(MoneyToWithdrawOrDeposit);

            if (tempWithdraw == -1) {
                System.out.println("Fault");
            }

            if (tempWithdraw < MoneyToWithdrawOrDeposit) {
                System.out.println("Insufficient funds, take only = " + tempWithdraw);
                System.out.println("Balance (" + userWorkingNowBank.getOwner() + ") = " + userWorkingNowBank.getAmount());
            } else {
                System.out.println("Balance (" + userWorkingNowBank.getOwner() + ") = " + userWorkingNowBank.getAmount());
            }
        }

        if (withdrawOrDeposit == 2) {
            int tempTopUp = userWorkingNowCard.topUp(MoneyToWithdrawOrDeposit);

            if (tempTopUp == -1) {
                System.out.println("fault (" + userWorkingNowBank.getOwner() + ")");
            }

            if (tempTopUp == -2) {
                System.out.println("Too much money to top up, repeat in part: " + MoneyToWithdrawOrDeposit);
                System.out.println("Balance (" + userWorkingNowBank.getOwner() + ") = " + userWorkingNowBank.getAmount());
            } else {
                System.out.println("Balance (" + userWorkingNowBank.getOwner() + ") = " + userWorkingNowBank.getAmount());
            }
        }
    }
}
