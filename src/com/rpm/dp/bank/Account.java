package com.rpm.dp.bank;

@SuppressWarnings("unused")
/*
 * реализация действий для мейна
 * @Author Mashina
 */
public class Account {
    private final String number;
    private final String owner;
    private int amount;

    Account(final String number, final String owner) {
        this.number = number;
        this.owner = owner;
    }

    Account(String number, String owner, int amount) {
        this.number = number;
        this.owner = owner;
        this.amount = amount;
    }

    public String getNumber() {
        return number;
    }

    String getOwner() {
        return owner;
    }

    int getAmount() {
        return amount;
    }

    /**
     * Снятие денег
     * Проверяет правильность введенной суммы денег
     *
     * @param amountToWithdraw сумма для снятия
     * @return -1 при отрицательной сумме,
     *         баланс при сумме больше, чем есть на балансе,
     *         сумму снятия
     */
    private int withdraw(int amountToWithdraw) {
        if (amountToWithdraw < 0) {
            return -1;
        }
        if (amountToWithdraw > amount) {
            final int amountToReturn = amount;
            amount = 0;
            return amountToReturn;
        }
        amount = amount - amountToWithdraw;
        return amountToWithdraw;
    }

    /**
     * Пополнение баланка
     * Проверяет правильность введенной суммы денег
     *
     * @param money сумма для пополнения
     * @return -1 при отрицательной сумме,
     *         -2 при сумме бльшей чем 100к
     *         сумму пополнения
     */
    private int deposit(int money) {
        if (money < 0) {
            return -1;
        }
        if (money > 100000) {
            return -2;
        }
        amount += money;
        return money;
    }

    /**
     * Внутренний класс карт
     * Inner class
     */
    public class Card {
        private final String number;

        Card(final String number) {
            this.number = number;
        }

        public String getNumber() {
            return number;
        }

        int withdraw(final int amountToWithdraw) {
            return Account.this.withdraw(amountToWithdraw);
        }

        int topUp(final int money) {
            return Account.this.deposit(money);
        }

    }
}
