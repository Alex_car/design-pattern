package com.rpm.dp.iceCreamCafe.decorators;

import com.rpm.dp.iceCreamCafe.object.Component;

public class WithoutTopping extends IceCreamDecorator {
    public WithoutTopping(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" - без топпинга");
    }
}
