package com.rpm.dp.iceCreamCafe.decorators;

import com.rpm.dp.iceCreamCafe.object.Component;

public abstract class IceCreamDecorator implements Component {
    private Component component;

    IceCreamDecorator(Component component) {
        this.component = component;
    }

    public abstract void afterTopping();

    @Override
    public void topping() {
        component.topping();
        afterTopping();
    }
}
