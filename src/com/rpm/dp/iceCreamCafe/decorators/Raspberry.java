package com.rpm.dp.iceCreamCafe.decorators;

import com.rpm.dp.iceCreamCafe.object.Component;

public class Raspberry extends IceCreamDecorator {
    public Raspberry(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" - добавлена малина");
    }
}
