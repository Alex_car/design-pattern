package com.rpm.dp.iceCreamCafe.decorators;

import com.rpm.dp.iceCreamCafe.object.Component;

public class Syrup extends IceCreamDecorator {
    public Syrup(Component component) {
        super(component);
    }

    @Override
    public void afterTopping() {
        System.out.println(" - добавлен сироп");
    }
}
