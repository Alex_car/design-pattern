package com.rpm.dp.iceCreamCafe;

import com.rpm.dp.iceCreamCafe.decorators.Raspberry;
import com.rpm.dp.iceCreamCafe.decorators.Syrup;
import com.rpm.dp.iceCreamCafe.decorators.WithoutTopping;
import com.rpm.dp.iceCreamCafe.object.IceCream;
import com.rpm.dp.iceCreamCafe.object.Component;
import com.rpm.dp.iceCreamCafe.object.Smoothie;
import com.rpm.dp.iceCreamCafe.object.Yoguгt;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Component iceCream;
        Component smoothie;
        Component yogurt;

        Random random = new Random();
        boolean showBorder = random.nextBoolean();

        if (showBorder) {
            iceCream = new Raspberry(new IceCream());
            smoothie = new Syrup(new Smoothie());
            yogurt = new WithoutTopping(new Yoguгt());
        } else {
            iceCream = new IceCream();
            smoothie = new Smoothie();
            yogurt = new Yoguгt();
        }

        iceCream.topping();
        smoothie.topping();
        yogurt.topping();

        iceCream = new Raspberry(new Syrup(new IceCream()));
        iceCream.topping();
    }
}
